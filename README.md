# Jekyll::Latex::Pdf

This gem uses kramdown and latex to generate pdf files.

## Introduction

I'm using latex for about 20 years now and jekyll for about 4.  I like latex and
the results we get for text processing.  And I like jekyll for its easy process
to generate great websites.

In this project I'll combine the best of both.  The kramdown converter which
will be used by the most jekyll projects (as far as I know) supports latex.
Thats great! But we can get much more out of latex with a view tricks.

The project grows about the features I use.  For example I wrote the optional
depencency to [jekyll-scholar](https://github.com/inukshuk/jekyll-scholar).  So
if it's enabled in your project, we use the latex way of generating bibliography
citations.  I didn't find any other package which will do so.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'jekyll-latex-pdf'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-latex-pdf

To make it work with jekyll, you need to add `- jekyll-latex-pdf` to the plugins
section in your `_config.yml` file of your jekyll site.

In `_config.yml` you need to add a section for the pdf config:
`pdf:
   pdf_engine: lualatex
   author: The Default Author
`
See below for other options and what the options control.

You need to have Latex installed on your system.  Default is to use `lualatex`
and `biber` which depends on your system how to install.  Look for texlive and
biblatex to see how it works.

If you want to use the `tikz` environment you additionally need to install
`pdf2svg` and `graphicsmagick`.

You may want to add `.latex-cache` (or whatever you set in your
`latex_cache_path`) and `assets/tikz/` to your `.gitignore`.  This is where we
store the latex files for faster recompiling if needed.

## Usage

Simply add `pdf: true` somewhere in your yaml header of the post.

In your post template use 

```
<a href="{{ page.pdf_url }}" target="_blank">PDF</a>
```

to get a link to the pdf version of your page.  Maybe you want to surround this
in an if clause if you have posts which should not be rendered to pdf too.

Please note that if you use a baseurl, this is not automatically included. In this case you should use: 
```
<a href="{{ page.pdf_url | prepend:site.baseurl }}" target="_blank">PDF</a>
```


## Configuration

We ship a latex template which is derived from the kramdown original to add
title, author and date from the post.  See section below.

jekyll-latex-pdf uses `lualatex` as default engine.  You can change this in
`_config.yml` by adding.  There you can also set a default author:

```
pdf:
   pdf_engine: pdflatex 
   author: Martin Kaffanke
   template: myowntemplate
```

All this variables can also be adjusted in the yaml header of your post.

Variables to customize are:

| Variable          | Description                                                                       |
|:------------------|:----------------------------------------------------------------------------------|
| `pdf_engine`      | Defaults to `lualatex` and can be changed to pdflatex, xelatex, ...               |
| `bib_engine`      | Defaults to `biber` and is used when `jekyll-scholar` is present.                 |
| `template`        | Defaults to `jekyll-latex-pdf`                                                    |
| `template_path`   | Is where we look for your own templates. Defaults to `_latex`.                    |
| `date_as_note`    | Is `false` by default.  See apa6                                                  |
| `tikz_path`       | Defaults to `assets/tikz`.  See tikz section.                                 |
| `latex_cache_dir` | Path where we hold the latex files.  Default: `.latex_cache`                      |
| `tikz_md5`        | Default: `false`.  Use md5 digest in filename to force browser reload on changes. |

Just set up a ticket on
(issues)[https://gitlab.com/grauschnabel/jekyll-latex-pdf/issues] to add feature
requests you need.

## Customize Templates

The default template is a good way to start.  Copy it to your `_latex` folder,
and adjust it.  Change the name and set the `template` in your config or the
yaml header of your post.  Your template in `_latex` will be prefered to the one
shiped with this gem if it has the same name.  Files should have the `.latex`
extension.

If you change the template when pdfs are allready created, make sure to delete
the `latex_cache_dir`, otherwise only changed posts will be recompiled with the
new template.

### jekyll-latex-pdf

This is the default template which was taken from the `kramdown` gem, but added
support for author, date and title, which is taken from the header of the post
or the _config.yml file.  There will also be used `lang` for babel and isodate
to support different languages.  (Only sv, de and en is supported at the moment,
just issue your language if this isn't enaugh.)

### apa7

Note: The apa6 is not removed for backwards compatibility. Read the
old docs if you need to work with that instead.

I use the `apa7` template mostly.  This requires the `jekyll-scholar` gem to be
added to your procject and enabled in the `_config.yml`.  A few more variables
can be set in the post (or globally in _config.yml).  The content of the extra
variables will not be parsed with kramdown. You may use latex notation here, or
just use plain text.  Not documented headers will be directly passed as is like
documented in the (apa7
class)[http://mirrors.ctan.org/macros/latex/contrib/apa7/apa7.pdf].

- *abstract*: Add your abstract to the post in the yaml header.  Abstract is
  required for apa, so if it is not present the excerpt will be used.  This does
  not work fine in some cases.  If so, put it into the yaml header may help.
- *shortitle*: Is now required to make it fine.
- *affiliation*: The authors affiliation is required with the apa6 class.
- *note*: was supporeted in apa6 and does not work in apa7. However I
   use this to set the date of the document. I use
   `\printdate{yyyy-mm-dd}`, so its converted to your language.  It is
   appended to the affiliation.
- *date_as_note*: When set to `true` this will add the posts date as note
  instead of the date.
- *authornote*
- *keywords*
- *journal*: Original uscase is the journal name.  You can use your website here
  if you want.
- *volume*
- *ccoppy*
- *copnum*

apa7 does not render the date by default. You can render the date in the note
field.  Let jekyll-latex-pdf do that for you by setting `date_as_note` to `true`
in the yaml header of your post or the `_config.yml`s pdf section for all.

New in version 0.6.3 the apa7 template support aditional preamble
statemens. This should be a list:

```
pdf:
  preamble:
    - \usepackage{Alegreya}
``` 

## Integration of other jekyll plugins:

### jekyll-scholar

Check the (jekyll-scholar
documentation)[https://github.com/inukshuk/jekyll-scholar] to figure out how
citation works.  Not everything is supported right now, send me an issue if you
need a feature which doesn't work correct for you.

Basically this ignores most of the jekyll-scholar configuration which is just
for the webpages it creates.  You need to define your citation styles and so on
in a customized template.

### jekyll-figure

Use this to create figure environments inside html.  With jekyll-latex-pdf you
get also the figure environment inside latex files.

jekyll-figure did not use label and reference, which is often used in latex
documents.  So I implemented this to jekyll-figure, see my (pull
request)[https://github.com/paulrobertlloyd/jekyll-figure/pull/7]. As long as
the pull isn't activated, you can use 

```
  gem 'jekyll-figure', git: 'https://github.com/grauschnabel/jekyll-figure.git', branch: 'figure_label'
```

in your jekyll sites Gemfile to have that feature activated for websites.

## Other extras

### Tikz

tikz is a graphics interface used in latex.  We add support for using tikz also
for html files, so you can use 

```
   {% tikz filename %}
     \tikz code goes here
   {% endtikz %}
```

On your page.  This will use latex to render the tikz code to a pdf which will
be converted to svg and png then.  This svg is included as object.  For latex,
the tikz code will go directly into the source file and will be rendered during
the rendering process.

You can also use tikzlibraries by using for example:

```
   {% tikz filename libraries: arrows snakes %}
   ...
```

You can adjust the `tikz_path` if you want to store the svg and png files
somewhere else.  The svg file will be used in your page by default, but if the
browser cannot render it, the png file will be used.

You need to have `pdf2svg` and `graphicsmagick` installed on your system.

## Different versions for html and latex.

Kramdowns (nomarkdown)[https://kramdown.gettalong.org/syntax.html#extensions]
extension is great for this.  Simple example:

```
{::nomarkdown type="latex"}
This should be written in latex, and will not be put in the html version.
{:/nomarkdown}

{::nomarkdown type="html"}
This will only be available in the HTML version of the page.  Use HTML instead
of markdown here.
{:/nomarkdown}
```

## Todo:

- add cite support in the abstract.

## Development

This packages is hardly in development at the moment.  Feel free to add feature
requests and issues.  Help is welcome.

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
