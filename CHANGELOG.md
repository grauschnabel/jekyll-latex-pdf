# Changelog

## Next

	
## [0.6.3]
    - Support additional preamble statements in the apa7 template


## [0.6.2]
### Added
    - Support for apa7.  (But we don't break apa6 it's just undocumented now.)
	
## [0.5.4]
### Added
    - Make links gray in apa6 pdf.
    - Making links absolute in pdf.
    
### Bugfix
    - Makeing links work in pdf.

## [0.5.3]
### Added
    - Use libraries for tikz pictures.
    - Use md5 in tikz filenames forces reload in browser when changed.
    - Added figref tag which was introducded by me to jekyll-figure
    - No need to rebuild if latex source didn't change.

## [0.5.1]
### Added
    - Configurable latex and tikz cache.
    
### Bugfix
    - trying to restore non used LiquidTags.

## [0.5.0]
### Added
    - Adding support for the jekyll-figure package.
    - Adding tkiz support, for HTML and PDF.
    - Moved to chache files inside project tree instead of tmp files.

    
## [0.4.2] - 2019-04-21
### Added 
    - Template search routine
    - apa6 template
    - some new config variables (see readme)

## [0.4.1] - 2019-04-19
### Fixed 
    - works again with filters on date in jekyll templates.
    - works again without jekyll-scholar.
    - wrong extname in some cases.
    - if permalink is set
