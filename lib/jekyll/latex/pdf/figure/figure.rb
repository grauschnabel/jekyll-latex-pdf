# frozen_string_literal: true

module Jekyll
  module Latex
    # This module generates pdf files using latex (must be installed on the system) and
    # should be highly configurable.
    module Pdf
      ## here we add support for jekyll-scholar, a great module for scientific writing.
      module Figure
        # Overrides the Figure tag from jekyll-figure
        class FigureTag < Liquid::Block

          include Utilities

          def initialize(tag_name, markup, tokens)
            @markup = markup
            super
          end

          def render(context)
            # Render any liquid variables
            markup = Liquid::Template.parse(@markup).render(context)

            # Extract tag attributes
            attributes = {}
            markup.scan(Liquid::TagAttributes) do |key, value|
              attributes[key] = value
            end

            @caption = attributes["caption"] if attributes.include? "caption"
            @label = attributes["label"] if attributes.include? "label"

            # Caption: convert markdown and remove paragraphs
            unless @caption.nil?
              figure_caption = @caption.gsub!(/\A"|"\Z/, "")
              figure_caption = Kramdown::Document.new(figure_caption).to_latex.strip
              figure_caption = "\\caption{#{figure_caption}}\n"
            end

            figure_main = super context

            # render figure
            figure_latex = nomarkdown_p "\\begin{figure}"
            figure_latex += nomarkdown_p "\\centering"
            figure_latex += figure_main.to_s
            figure_latex += nomarkdown_p figure_caption.to_s
            figure_latex += nomarkdown_p "\\label{#{@label}}" unless @label.nil?
            figure_latex + nomarkdown_p("\\end{figure}")
          end

        end
      end
      TempLiquid.register_tag("figure", Figure::FigureTag)
    end
  end
end
