# frozen_string_literal: true

require "open3"

module Jekyll
  module Latex
    module Pdf
      # The Latex Class will create the tex file and compile it to the pdf file.
      class Latex < Jekyll::Document

        include Utilities

        attr_accessor :source, :latex
        attr_reader :pdf_file
        def initialize(source, site, page, data, name)
          @kramdowndata = data
          @site = site
          @page = page
          @name = name
          @engine = @site.config["pdf"]["pdf_engine"] # @options.options[:pdf_engine]
          @bibengine = @site.config["pdf"]["bib_engine"] # @options.options[:bib_engine]

          @source = source
        end

        def find_template
          template = @kramdowndata.data[:template]

          template.concat(".latex") unless template.end_with?(".latex")

          t = File.expand_path(File.join(@site.config["pdf"]["template_path"], template))
          if File.file? t
            return t
          else
            t = File.expand_path(File.join(@site.config["pdf"]["default_template_path"], template))
            if File.file? t
              return t
            else
              Jekyll.logger.error "jekyll-latex-pdf", "Could not find template #{t}."
            end
          end
        end

        def prepare_latex
          TempLiquid.run do
            info = {
              registers: {site: @site, page: @site.site_payload["page"]},
              strict_filters: true,
              strict_variables: true,
            }
            
            template = Liquid::Template.parse(@source)
            template.warnings.each do |e|
              Jekyll.logger.warn "Liquid Warning:",
                                 LiquidRenderer.format_error(e, path || document.relative_path)
            end
            prep_latex = template.render!(@site.site_payload, info)
            

            options = {"data" => @kramdowndata.data,
                       "template" => find_template}
            @latex = Kramdown::Document.new(prep_latex, options).to_latex
          end
        end

        def prepare_abstract
          # only when excerpt is present
          unless @kramdowndata.data.key? :abstract
            Jekyll.logger.debug "Abstract missing. This may be ok for your latex template."
            if @kramdowndata.data.key? :excerpt
              Jekyll.logger.debug "Trying to get the abstract from the excerpt."
              abstract, _, @source = @source.to_s.partition(excerpt_separator)
              @kramdowndata.add(abstract: Kramdown::Document.new(abstract).to_latex)
             end
           end
        end

        def prepare_date_as_note
          @kramdowndata.add(note: "\\printdate{#{@kramdowndata.data[:date_str]}}")
        end

        def prepare_tikz
          @kramdowndata.add(tikzlibraries: Tikz::TikzLibraries.render)
        end

        def latex_same?(file, code)
          return false unless File.exist? file

          File.open(file, 'r') do |latexfile|
            latexfile.read == code
          end
        end

        def prepare
          prepare_abstract
          prepare_date_as_note if @site.config["pdf"]["date_as_note"]
          prepare_tikz
          prepare_latex

          basename = File.basename(@name, ".*")
          @tempdir = File.join(Dir.pwd, @site.config["pdf"]["latex_cache_path"], "pdf", basename)
          FileUtils.mkdir_p @tempdir
          @latexfile = basename + ".tex"

          # stop here, if it does not need to compile
          latex_full_path = File.join(@tempdir, @latexfile)

          if latex_same? latex_full_path, latex
            @no_need_to_compile = true
            return
          end

          File.open(latex_full_path, "w:UTF-8") do |f|
            f.write(latex)
          end

          add_bibliography
        end

        def add_bibliography
          if @kramdowndata.data.key? :bibtex_files
            @kramdowndata.data[:bibtex_files].each do |bibfile|
              bib_target = File.join(@tempdir, bibfile)
              if File.exist?(bib_target)
                unless FileUtils.compare_file(bibfile, bib_target)
                  FileUtils.cp(bibfile, bib_target)
                end
              else
                FileUtils.mkdir_p File.dirname(bib_target)
                FileUtils.cp(bibfile, bib_target)
              end
            end
          end
        end

        def prepare_cmds
          cmds = [[@site.config["pdf"]["pdf_engine"],
                   "--output-format=pdf",
                   "--interaction=batchmode",
                   @latexfile]]
          # biber if bibfiles given
          if @kramdowndata.data.key? :bibtex_files
            cmds << [@site.config["pdf"]["bib_engine"],
                     File.basename(@latexfile, File.extname(@latexfile))]
            cmds << cmds[0]
          end
          cmds
        end

        def compile
          prepare
          status = 0
          if @no_need_to_compile.nil?
            cmds = prepare_cmds

            Jekyll.logger.debug "jekyll-latex-pdf", "compiling in tempdir: #{@tempdir}"

            _out, status = run_cmds cmds, @tempdir
          end

          if 0 == status
            @pdf_file = File.join(@tempdir, File.basename(@latexfile, ".*") + ".pdf")
          else
            Jekyll.logger.error "jekyll-latex-pdf",
                                "Error when trying to run #{@site.config['pdf']['pdf_engine']}."
            Jekyll.logger.error "jekyll-latex-pdf", "status: #{status}"
            Jekyll.logger.error "jekyll-latex-pdf",
                                "Hint: Try to run #{site.config['pdf']['pdf_engine']} \
              inside #{@tempdir} by hand."
          end
          status
        end

      end
    end
  end
end
