# frozen_string_literal: true

module Jekyll
  module Latex
    # This module generates pdf files using latex (must be installed on the system) and
    # should be highly configurable.
    module Pdf
      # The generator acutally generates pdf files for posts with `pdf: true` in the yaml header.
      class Generator < Jekyll::Generator

        safe true
        priority :lowest

        attr_reader :config

        def generate(site)
          @site, @config = site, Defaults.defaults.merge(site.config["pdf"] || {})

          @site.config["pdf"].merge! @config
          @site.config["exclude"] << @site.config["pdf"]["latex_cache_path"].to_s + File::SEPARATOR

          @site.posts.docs.each do |post|
            @site.pages << Document.new(@site, post) if post.data["pdf"]
          end
        end

      end
    end
  end
end
