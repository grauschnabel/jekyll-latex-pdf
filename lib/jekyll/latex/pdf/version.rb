# frozen_string_literal: true

module Jekyll
  module Latex
    module Pdf
      VERSION = "0.6.3"
    end
  end
end
