# frozen_string_literal: true

lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "jekyll/latex/pdf/version"

Gem::Specification.new do |spec|
  spec.name          = "jekyll-latex-pdf"
  spec.version       = Jekyll::Latex::Pdf::VERSION
  spec.authors       = ["Martin Kaffanke"]
  spec.email         = ["martin@kaffanke.at"]

  spec.summary       = "Crates pdfs using just latex for jekyll posts.  (needs kramdown)"
  spec.description   = "Create PDFs for download from your posts, using latex flexible and comfortable."
  spec.homepage      = "https://gitlab.com/grauschnabel/jekyll-latex-pdf"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r!^(test|spec|features)/!)
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r!^exe/!) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.2"
  spec.add_development_dependency "method_source", "~> 1.0"
  spec.add_development_dependency "pry", "~> 0.14"
  spec.add_development_dependency "pry-doc", "~> 1.2.0"
  spec.add_development_dependency "rake", "~> 13"
  spec.add_development_dependency "rubocop", "~> 1.23"
  spec.add_development_dependency "jekyll-scholar", "~> 7.0"
  #spec.add_development_dependency "minitest", "~> 5.0.0"
  #spec.add_development_dependency "rouge"
  #spec.add_development_dependency "stringex", "~> 1.5.1"

  spec.add_runtime_dependency "jekyll", "~> 4.2"
  spec.add_runtime_dependency "kramdown", "~> 2.3"
end
